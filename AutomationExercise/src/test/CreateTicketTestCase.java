package test;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import test.data.Issue;
import test.data.User;
import test.pageobjects.DashboardPage;
import test.pageobjects.IssuePage;
import test.pageobjects.NewIssuePopup;

public class CreateTicketTestCase {
	private static WebDriver driver;
	WebElement element;

	@BeforeClass
	public static void openBrowser(){
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/**
	 * Only one successful test, according to the task
	 */
	@Test
	public void success_CreateTicket(){
		// Given I am logged in to the Jira Dashboard as a test user
		DashboardPage dashboard = new DashboardPage(driver, new User());

		// When I click "Create" button in the dashboard menu
		NewIssuePopup popup = dashboard.clickCreateIssue();

		// And I fill necessary fields for a new issue in the modal pop-up
		popup.fillFields(new Issue());

		// And I click "Create" button in the modal pop-up
		dashboard = popup.createIssue();

		// Then I should see success message
		String issueKey = dashboard.assertIssueCreatedMessage();

		// And a new issue is available
		IssuePage issuePage = new IssuePage(driver, true, issueKey);
		issuePage.assertIssueExists();
	}

	@AfterClass
	public static void closeBrowser(){
		driver.quit();
	}
}
