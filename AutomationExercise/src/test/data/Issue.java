package test.data;

/**
 * Stub class for future expansion and test data management
 */

public class Issue {
	public String project = "TST";
	public String type = "Bug";
	public String summary = "Test automation is not working";
	public String description = "Keep calm and follow the white rabbit";
}
