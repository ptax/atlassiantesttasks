package test.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	private final WebDriver driver;

	private By usernameLocator = By.id("username");
	private By passwordLocator = By.id("password");
	private By loginButtonLocator = By.id("login-submit");

	public LoginPage(WebDriver driver) {
		this.driver = driver;
        if (!driver.getCurrentUrl().contains("https://id.atlassian.com/login?")) {
            throw new IllegalStateException("This is not the login page");
        }
    }

	public LoginPage typeUsername(String username) {
		driver.findElement(usernameLocator).sendKeys(username);
		return this;    
	}

	public LoginPage typePassword(String password) {
		driver.findElement(passwordLocator).sendKeys(password);
		return this;    
	}

	public DashboardPage submitLogin() {
		driver.findElement(loginButtonLocator).submit();
		// for the scope of a given test task we assume that we are logging in only to Jira
		return new DashboardPage(driver);
	}

	public LoginPage submitLoginExpectingFailure() {
		driver.findElement(loginButtonLocator).submit();
		return new LoginPage(driver);   
	}

	public DashboardPage loginAs(String username, String password) {
		typeUsername(username);
		typePassword(password);
		return submitLogin();
	}
}