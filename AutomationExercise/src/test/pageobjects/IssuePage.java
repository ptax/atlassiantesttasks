package test.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IssuePage {
	private final WebDriver driver;
	
	@FindBy(id = "key-val")
	private WebElement breadcrumbsIssueKey;

	public IssuePage(WebDriver driver, boolean doOpen, String key) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
		if (doOpen) {
			open(key);
			assertIssueExists();
		}
	}

	public void open(String key) {
		driver.get("https://jira.atlassian.com/browse/" + key);
	}

	public void assertIssueExists() {
		Assert.assertTrue(breadcrumbsIssueKey.isDisplayed() == true);
	}
}
