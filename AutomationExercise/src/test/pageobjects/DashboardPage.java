package test.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;

import test.TestRunner;
import test.data.User;

public class DashboardPage {
	private final WebDriver driver;

	@FindBy(css = "#user-options a.login-link")
	private WebElement loginButton;
	@FindBy(id = "create_link")
	private WebElement createIssueButton;
	@FindBy(css = "a.issue-created-key.issue-link")
	private WebElement newIssueLink;

	public DashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public DashboardPage(WebDriver driver, User user) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
		this.open();
		this.loginAs(user.email, user.password);
	}

	public void open() {
		driver.get("https://jira.atlassian.com/secure/Dashboard.jspa");
	}

	public DashboardPage loginAs(String name, String pass) {
		loginButton.click();
		LoginPage lp = new LoginPage(driver);
		DashboardPage newDashboard = lp.loginAs(name, pass);
		return newDashboard;
	}

	public NewIssuePopup clickCreateIssue() {
		createIssueButton.click();
		return new NewIssuePopup(driver);
	}

	public String assertIssueCreatedMessage() {
		WebDriverWait wait = new WebDriverWait(driver, TestRunner.DELAY);
		wait.until(ExpectedConditions.visibilityOf(newIssueLink));
		return newIssueLink.getAttribute("data-issue-key");
	}
}
