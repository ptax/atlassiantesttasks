package test.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import test.TestRunner;
import test.data.Issue;

public class NewIssuePopup {
	private final WebDriver driver;

	private By selectProjectDropDownLocator = By.id("project-field");
	private By selectTypeDropDownLocator = By.id("issuetype-field");
	private By summaryFieldLocator = By.id("summary");
	private By descFieldLocator = By.id("description");
	private By submitButtonLocator = By.id("create-issue-submit");

	public NewIssuePopup(WebDriver driver) {
		this.driver = driver;
	}

	public NewIssuePopup selectProject(String prj) {
		WebDriverWait wait = new WebDriverWait(driver, TestRunner.DELAY);
		WebElement projectDropDown = wait.until(ExpectedConditions.elementToBeClickable(selectProjectDropDownLocator));
		projectDropDown.click();
		projectDropDown.clear();
		projectDropDown.sendKeys(prj);
		projectDropDown.sendKeys(Keys.RETURN);		
		return this;    
	}

	public NewIssuePopup selectIssueType(String type) {
		WebDriverWait wait = new WebDriverWait(driver, TestRunner.DELAY);
		WebElement typeDropDown = wait.until(ExpectedConditions.elementToBeClickable(selectTypeDropDownLocator));
		typeDropDown.click();
		typeDropDown.clear();
		typeDropDown.sendKeys(type);
		typeDropDown.sendKeys(Keys.RETURN);
		return this;    
	}

	public NewIssuePopup typeSummary(String str) {
		WebDriverWait wait = new WebDriverWait(driver, TestRunner.DELAY);
		WebElement summaryField = wait.until(ExpectedConditions.elementToBeClickable(summaryFieldLocator));
		summaryField.click();
		summaryField.clear();
		summaryField.sendKeys(str);
		return this;		
	}

	public NewIssuePopup typeDescription(String str) {
		WebDriverWait wait = new WebDriverWait(driver, TestRunner.DELAY);
		WebElement descField = wait.until(ExpectedConditions.elementToBeClickable(descFieldLocator));
		descField.click();
		descField.clear();
		descField.sendKeys(str);
		return this;
	}

	public void fillFields(Issue issue) {
		selectProject(issue.project);
		selectIssueType(issue.type);
		typeSummary(issue.summary);
		typeDescription(issue.description);
	}

	public DashboardPage createIssue() {
		driver.findElement(submitButtonLocator).click();
		return new DashboardPage(driver);
	}
}
